<?php
ob_start();
session_start();
if (isset($_SESSION['akun_id'])) header("location: index.php");
include "config.php";

if (isset($_POST['submit_confirm'])) {
    $token=$_GET['t'];
    $password = hash('sha256',md5($_POST['password']));

    // $token='kodetergantipassword';
    $sql = mysqli_query($conn, "UPDATE user SET password='$password' WHERE token='$token'");
    $sql;
    if($sql){
        echo '<div class="alert alert-warning" style="text-align:center;">
        Password anda berhasil diubah, silakan  <a href="login.php"> login</a> dengan password baru anda
        </div>';
    }

   
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reset Password</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <style>
        .bg-login {
            background-image: url("https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>

</head>

<body class="bg-gradient-secondary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-3 d-none d-lg-block"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Masukan email dan username anda untuk melakukan perubahan password</h1>
                                    </div>
                                    <form class="user login100-form validate-form" action="" method="post">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" class="form-control" id="password" required>
                                            <input type="checkbox" class="form-checkbox"> Show password
                                        </div>
                                        <?php
                                        if (isset($_GET['login-gagal'])) {
                                        ?>
                                            <div>
                                                <p style="color:red;">Periksa kembali username atau password yang anda masukan!</p>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                        <button class="btn btn-primary btn-user btn-block" type="submit" name="submit_confirm">
                                            Kirim
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){		
		$('.form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#password').attr('type','text');
			}else{
				$('#password').attr('type','password');
			}
		});
	});
    </script>

</body>

</html>
<?php
mysqli_close($conn);
ob_end_flush();
?>