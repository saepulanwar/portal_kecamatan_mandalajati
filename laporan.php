<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 5px;
        font-size: 12px;
        padding: 10px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    .table-title {
        padding-top: 10px;
    }

    .info a {
        color: #ffff;
    }

    /* CSS link color */

    .horizontal {
        overflow-x: scroll;
        overflow-y: hidden;
        white-space: nowrap;
        width: 100%;
    }
    p{
        font-size:12px;
    }

    /* #col_status{
        cursor:pointer;
    }        
    #col_status:hover{
        background-color:yellow;
    } */
    #btnExport {
        background-color: green;
        color: white;
        border-radius: 5px;
        padding: 5px;
    }
</style>

<?php
if (!isset($_SESSION['akun_id'])) header("location: login.php");
// include "config.php";
include("koneksi.php");
$nama_username = $_SESSION['akun_username'];

if ($_SESSION['akun_level'] == "admin") {
    if(isset($_POST['submit'])){
        if($_POST['filter_bulan']=="Semua"){
            // filter per tahun
            $tahun=$_POST['filter_tahun'];
            $sql = "SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu,id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen EKTP' AS jns_data
            FROM buat_ektp WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen KK' AS jns_data
            FROM buat_kk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>3 THEN 'Lambat' ELSE 'Normal' END AS nilai,3 AS waktu_normal, 'Dokumen Surat Ahli Waris' AS jns_data
            FROM buat_saw WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen Surat Izin Menetap' AS jns_data
            FROM buat_sim WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen SKCK' AS jns_data
            FROM buat_skck WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>2 THEN 'Lambat' ELSE 'Normal' END AS nilai, 2 AS waktu_normal, 'Dokumen Surat Pindah Keluar' AS jns_data
            FROM buat_spk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>7 THEN 'Lambat' ELSE 'Normal' END AS nilai, 7 AS waktu_normal, 'Dokumen Surat Domisili' AS jns_data
            FROM buat_surat_domisili WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun'";
    $result = $connection->query($sql);
    $jumlahData=mysqli_num_rows($result);
        }else{
            // filter per bulan
            $tahun=$_POST['filter_tahun'];
            $bulan=$_POST['filter_bulan'];
            $sql = "SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu,id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen EKTP' AS jns_data
            FROM buat_ektp WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen KK' AS jns_data
            FROM buat_kk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>3 THEN 'Lambat' ELSE 'Normal' END AS nilai,3 AS waktu_normal, 'Dokumen Surat Ahli Waris' AS jns_data
            FROM buat_saw WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen Surat Izin Menetap' AS jns_data
            FROM buat_sim WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen SKCK' AS jns_data
            FROM buat_skck WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>2 THEN 'Lambat' ELSE 'Normal' END AS nilai, 2 AS waktu_normal, 'Dokumen Surat Pindah Keluar' AS jns_data
            FROM buat_spk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'
            UNION ALL 
            SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
            CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>7 THEN 'Lambat' ELSE 'Normal' END AS nilai, 7 AS waktu_normal, 'Dokumen Surat Domisili' AS jns_data
            FROM buat_surat_domisili WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL AND YEAR(tanggal_input)='$tahun' AND MONTH(tanggal_input)='$bulan'";
    $result = $connection->query($sql);
    $jumlahData=mysqli_num_rows($result);
        }
    }else{
    $sql = "SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu,id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen EKTP' AS jns_data
    FROM buat_ektp WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen KK' AS jns_data
    FROM buat_kk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>3 THEN 'Lambat' ELSE 'Normal' END AS nilai,3 AS waktu_normal, 'Dokumen Surat Ahli Waris' AS jns_data
    FROM buat_saw WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen Surat Izin Menetap' AS jns_data
    FROM buat_sim WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>5 THEN 'Lambat' ELSE 'Normal' END AS nilai, 5 AS waktu_normal, 'Dokumen SKCK' AS jns_data
    FROM buat_skck WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>2 THEN 'Lambat' ELSE 'Normal' END AS nilai, 2 AS waktu_normal, 'Dokumen Surat Pindah Keluar' AS jns_data
    FROM buat_spk WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL
    UNION ALL 
    SELECT abs(datediff(tanggal_input, tanggal_proses)) AS waktu, id_data,
    CASE WHEN abs(datediff(tanggal_input, tanggal_proses))>7 THEN 'Lambat' ELSE 'Normal' END AS nilai, 7 AS waktu_normal, 'Dokumen Surat Domisili' AS jns_data
    FROM buat_surat_domisili WHERE (status='Diterima' OR status='Ditolak') AND tanggal_proses IS NOT NULL";
        $result = $connection->query($sql);
        $jumlahData=mysqli_num_rows($result);
        
    }
    $arrLambat=array();
        foreach($result as $val=>$v){
            if($v['nilai']=="Lambat"){
                $arrLambat[]=$v['nilai'];
            }
        }
        $jmlLambat=count($arrLambat);
    // $result;
    $curYear=date("Y");
    $arrBulan=['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
?>
    <div class="row">
        <div class="col-md-6">
            <!-- <select id="mylist" onchange="myFunction()" class='form-control'>
            <option value="">Semua</option>
            <?php 
            // foreach ($result as $data => $value) {
            //     echo "<option value='".trim($value['jns_data'], " ")."'>".trim($value['jns_data']," ")."</option>";
            // }
            ?>
            </select> -->
            
        </div>
        <div class="col-md-6">
            <form action="" method="POST">
                <div class="row" style="margin-right:-70px;">
                    <div class="col-md-6">
                        <select name="filter_bulan" id="filter_bulan" class="form-control">
                            <option value="Semua">-- Semua Bulan --</option>
                            <?php
                            for($a=0; $a<12; $a++){
                                echo "<option value='".($a+1)."'>".$arrBulan[$a]."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="filter_tahun" id="filter_tahun" class="form-control">
                          <?php                    
                            $tahun = date("Y");
                            for ($i = $tahun; $i >= $tahun - 10; $i--) {
                                echo "<option value='" . $i . "'>" . $i . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3" style="text-align:left;">
                        <button type="submit" name="submit" class="btn btn-primary">Filter</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="container-fluid" style="background-color: white; border-radius: 10px;">
        <br>
        <table class="table" id="myTable">
            <tr>
                <th>No</th>
                <th>Jenis Penjauan</th>
                <th>Waktu Normal</th>
                <th>Waktu Proses</th>
                <th>Kecepatan Proses</th>
            </tr>
            <?php
            $i = 1;
            foreach ($result as $data => $value) {
            ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo trim($value['jns_data']," ");?></td>
                    <td><?php echo $value['waktu_normal'] ?> hari</td>
                    <td><?php echo $value['waktu'] ?> hari</td>
                    <td style="background-color:<?php if ($value['nilai'] == 'Lambat') {
                                                    echo 'red';
                                                } ?>"><?php echo $value['nilai'] ?></td>
                </tr>

        <?php
                $i++;
            }
        }
        ?>
        </table>
        <p><?php 
            if(isset($_POST['submit'])){
                if($_POST['filter_bulan']=="Semua"){
                    echo "Jumlah data tahun ".$tahun." : <b>".$jumlahData."</b>";
                }else{
                    echo "Jumlah data ".$arrBulan[$bulan-1]." ".$tahun." : <b>".$jumlahData."</b>";
                }
            }else{
                    echo "Jumlah data total : <b>".$jumlahData."</b>";
            }?>
            || Jumlah Lambat : <?php echo "<b>".$jmlLambat."</b>"; ?>
            || Jumlah Normal : <?php echo "<b>".($jumlahData-$jmlLambat)."</b>"; ?>
        </p>

    </div>
    <br>
    <div class="smallfont">
        <!-- <a href="javascript:window.print()" class="link" style="font-family: Verdana; font-size: 10px">
			<img src="images/icon_printer.jpg" border="0" alt="Cetak"> Cetak PDF
		</a>|| -->
        <a href="" class="small" id="btnExport" onclick="fnExcelReport();">Cetak Excel</a>
        <!-- <button class="small" id="btnExport" onclick="fnExcelReport();">Excel </button> -->
    </div>
    <script>
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange;
            var j = 0;
            tab = document.getElementById('myTable'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "REPORT_RECON.xls");
            } else //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

        function myFunction() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("mylist");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
                }       
            }
        }
    </script>
