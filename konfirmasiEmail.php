<?php
ob_start();
session_start();
if (isset($_SESSION['akun_id'])) header("location: index.php");
include "config.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (isset($_POST['submit_confirm'])) {
    $email = $_POST['email'];
    $username = $_POST['username'];
    $token=hash('sha256', md5(date('Y-m-d'))) ;
    // $token='kodetergantipassword';
    $sql_email = mysqli_query($conn, "SELECT * FROM user WHERE email = '$email'");
    $sql_username = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");


    if (mysqli_num_rows($sql_email) > 0 && mysqli_num_rows($sql_username) > 0) {
        $sql2 = mysqli_query($conn, "UPDATE user SET token='$token' WHERE email='$email' AND username='$username'");
        $sql2;
        // EMAIL

        require 'vendor/autoload.php';

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'horigan12@gmail.com';                 // SMTP username
            $mail->Password = '@75912612An';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('horigan12@gmail.com', 'PORTAL KECAMATAN MANDALAJATI');
            $mail->addAddress($email);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Konfirmasi Perubahan Password';
            $mail->Body    = '<div class="row">
                                    <p>Konfirmasi email anda pada link berikut untuk melakukan perubahan password:</p>
                                    <a href="http://localhost/portal_kecamatan_mandalajati/gantiPassword.php?t=' . $token . '">http://localhost/portal_kecamatan_mandalajati/gantiPassword.php?t=' . $token . '</a>
                                </div>';
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';



            $mail->send();
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
        echo '<div class="alert alert-warning" style="text-align:center;">
        Email konfirmasi telah dikirimkan, silakan cek <a href="mail.google.com"> email</a>
        </div>';
    }else if(mysqli_num_rows($sql_email) == 0 && mysqli_num_rows($sql_username) == 0){
        echo '<div class="alert alert-warning" style="text-align:center;">
        Email dan Username tidak terdaftar, Silakan <a href="register.php"> daftar akun</a>
        </div>';
    }else if(mysqli_num_rows($sql_username) == 0){
        echo '<div class="alert alert-warning" style="text-align:center;">
            Username tidak terdaftar, Silakan <a href="register.php"> daftar akun</a>
      </div>';
    } else if(mysqli_num_rows($sql_email) == 0) {
        echo '<div class="alert alert-warning" style="text-align:center;">
            Email tidak terdaftar, Silakan <a href="register.php"> daftar akun</a>
      </div>';
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PORTAL - Konfirmasi Email</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <style>
        .bg-login {
            background-image: url("https://images.unsplash.com/photo-1499750310107-5fef28a66643?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80");
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>

</head>

<body class="bg-gradient-secondary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-3 d-none d-lg-block"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Masukan email dan username anda untuk melakukan perubahan password</h1>
                                    </div>
                                    <form class="user login100-form validate-form" action="" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" name="email" placeholder="Enter Email...">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" name="username" placeholder="Enter Username...">
                                        </div>
                                        <?php
                                        if (isset($_GET['login-gagal'])) {
                                        ?>
                                            <div>
                                                <p style="color:red;">Periksa kembali username atau password yang anda masukan!</p>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                        <button class="btn btn-primary btn-user btn-block" type="submit" name="submit_confirm">
                                            Kirim
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
<?php
mysqli_close($conn);
ob_end_flush();
?>